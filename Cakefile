fs = require('fs')
exec = require('child_process').exec
mkdirp = require('mkdirp')
path = require('path')
mkdirp = require('mkdirp')
glob = require('glob')


# Globals
convert_path = "python ../converter/goo_convert/convert.py"
converter_options = [
	'prettyprint'
	'binaryfile'
	'log-file convert.log'
	'debug'
	'preserve-urls'
	'shader casinoShader'
	'layered-lightmaps'
	'ambient2lightmap'
]

tojs_path = "python ../converter/goo_convert/to_js.py"
goo_path = "../GooJS"
deploy_path = "/Volumes/labs.gooengine.com 1/casinofloor"

# Cake

option '-f', '--fbx [FILE]', 'The fbx file to import'
option '-d', '--deployfolder [FILE]', 'Folder on goodemos.com to deploy to'
option '-i', '--includebinaries', 'Include texture and mesh data when deploying'


task 'import', 'Import a new fbx, e.g. \'cake --fbx ~/Downloads/gooEngine_fbx_test_v013e.fbx import\'', (options)->
	sceneDir = './resources/scene'
	runCommand "rm -rf #{sceneDir}/*", ->
		fbxfile = options.fbx
		conv_opts = "--#{converter_options.join(" --")}"
		runCommand "#{convert_path} #{fbxfile}  #{conv_opts} #{sceneDir}", ->
			console.log "Loaded scene"
			invoke 'modify'
			console.log "Copied manual additions"
			invoke 'copytextures'
			console.log "Copied textures"
			fixPlants()
			console.log "Fixed plants"


task 'modify', 'Copy manual modifications to scene', ->
	base = './resources/scene_modifications/'
	copyLibs base, './resources/scene', ['**/*.*']
	
	# Add entities
	glob '**/*.entity', {cwd: base}, (err, files) ->
		entityRefs = []
		for file in files 
			entityRefs.push file
		if entityRefs.length>0
			addEntitiesToScene(entityRefs)

task 'copytextures', 'Copy all the textures used in the scene, to the scene', ->
	base = './resources/scene/'
	glob '**/*.texture', {cwd: base}, (err, files) ->
		for file in files 
			textureConfig = JSON.parse(fs.readFileSync(path.resolve(base, file)))
			if textureConfig.url
				textureName = textureConfig.url.split('/').pop()
				try
					copyFile "./resources/scene_modifications/images/#{textureName}", path.resolve(base, textureConfig.url)
				catch e
					try 
						copyFile "./resources/all_texture_images/#{textureName}", path.resolve(base, textureConfig.url)
					catch e
						console.error "WARNING: #{textureName} is missing"


task 'merge', 'Convert a new fbx and add entities to the main scene', (options)->
	fbxfile = options.fbx
	namespace = fbxfile.split('/').pop()
	conv_opts = "--#{converter_options.join(" --")} --namespace #{namespace}"
	runCommand "#{convert_path} #{fbxfile} ./resources/scene #{conv_opts}", ->
		console.log "Loaded scene"
		newScene = JSON.parse(fs.readFileSync("./resources/scene/#{namespace}/test.scene"))
		addEntitiesToScene(newScene.entityRefs)



task 'scene.js', 'Stuff the scene tree into a .js file', ->
	runCommand "#{tojs_path} ./resources/scene ./resources/scene.js -p", ->
		console.log "Created JS file"


task 'minified', 'Gopy the minified engine from the GooJS repo (just make sure you minified the latest engine)', ->
	copyFile("#{goo_path}/minified/goo/goo.js", 'js/goo/goo.js')


task 'build', 'Build a bundle', (options) ->
	fileIn = '.'
	includes = [
		'index.html', 
		'resources/*.*'
		'resources/audio/*.*'
		'resources/water/*.*'

		'resources/scene/**/*.dat'
		'resources/scene/**/*.dds'
		'resources/scene/**/*.jpg'
		'resources/scene/**/*.png'

		'js/**/*.js'
		'style/**/*.png'
		'style/**/*.gif'
		'style/**/*.otf'
	]
	output = 'build'

	runCommand "rm -rf #{output}", ->
		copyLibs fileIn, path.resolve(output, fileIn), includes
		runCommand "coffee -cbo #{output}/#{fileIn} #{fileIn}", ->
			console.log "Compiled coffeescript" 			
		convertLess fileIn, path.resolve(output, fileIn), ['style/**/*.less']

task 'deploy', 'Deploy to goodemos e.g. cake -d august01 -i deploy', (options)->
	console.log "Deploying"
	console.log "!!! Did you remember to copy the new minified engine, and update the settings in app.coffee?"
	fileIn = 'build'
	output = "#{deploy_path}/#{options.deployfolder or ''}"
	includes = ['**/*.*']
	if options.includebinaries then excludes = []
	else excludes = ['png', 'jpg', 'jpeg','tga','dds','dat']
	copyLibs fileIn, output, includes, excludes, true
	# console.log "Copied files"

task 'plants', 'Fix plants', (options)->
	fixPlants()

task 'characterLightMaps', 'Fix character lightmaps', (options)->
	fixCharacterLightmaps()


# Utilities
endsWith = (str, suffix)-> str.indexOf(suffix, str.length - suffix.length) != -1

copyFile = (source, target, force=false) ->
	indata = fs.readFileSync source
	try
		mkdirp.sync path.dirname(target)
	catch e
		if not force
			throw e

	fs.writeFileSync target, indata

copyLibs = (base, target, includes, exclude_types=[], force=false) ->
	base = path.resolve(base)
	target = path.resolve(target)
		
	if includes.length > 1
		pathsToInclude = '{'+includes.join(',')+'}'
	else
		pathsToInclude = includes[0]

	glob pathsToInclude, {cwd: base}, (err, files) ->
		totalCount = files.length
		copiedCount = 0
		for file in files when file.split('.').pop() not in exclude_types
			#console.log "Copying",path.resolve(base, file), path.resolve(target, file)
			process.stdout.write "Copying file #{++copiedCount} of #{totalCount} \r"
			copyFile path.resolve(base, file), path.resolve(target, file), force
		process.stdout.write "\n"
		console.log "Copy complete!"


runCommand = (cmd, callback) ->
	exec cmd, (error, stdout, stderr) ->
		if error != null
			console.log stdout
			console.log stderr
			console.log 'Command failed: ' + cmd
			process.exit(1)
		if callback
			callback()		

convertLess = (base, target, includes) ->
	base = path.resolve(base)
	target = path.resolve(target)
	if includes.length > 1
		pathsToInclude = '{'+includes.join(',')+'}'
	else
		pathsToInclude = includes[0]

	glob pathsToInclude, {cwd: base}, (err, files) ->
		for file in files
			inputfile = path.resolve(base, file)
			outputfile = path.resolve(target, file.replace('.less', '.css'))
			#console.log "Converting less #{inputfile}, #{outputfile}"
			mkdirp.sync path.dirname(outputfile)
			runCommand "lessc #{inputfile} > #{outputfile}", ->
				console.log "Compiled #{inputfile}"

fixPlants = ()->

	for pattern in ['plant', 'palm', 'flower']
		glob "resources/scene/**/*#{pattern}*.material", {}, (err, files) ->
			for file in files
				updateFile file, (data)->
					data.blendState = blending: 'customBlending'
					data.cullState = enabled:false


fixCharacterLightmaps = ()->
	rootPath = "resources/scene"
	glob "#{rootPath}/**/*_idle/textures/*lightMap.dds", {}, (err, files) ->
		for file in files
			m = /casino_(wo)?man_[a-z]+_(.+).dds$/.exec(file)
			if m
				path = "#{m[1] or ''}man_idle/textures"
				lightMapRef = "#{path}/casino_#{m[1] or ''}man_#{m[2]}.texture"
				#console.log "File is #{file}, #{m.join(',')}"
				console.log "Creating lightmap texture #{lightMapRef}"
				texture = 
					url:"#{path}/#{file.split('/').pop()}"

				lightMapFile = "#{rootPath}/#{lightMapRef}"
				fs.writeFileSync lightMapFile, JSON.stringify(texture, null, '\t')

addEntitiesToScene = (entityRefs)->
	updateFile "./resources/scene/test.scene", (scene)->
		for entityRef in entityRefs
			if entityRef not in scene.entityRefs
				console.log "Adding entity #{entityRef}"
				scene.entityRefs.push entityRef


updateFile = (file, fix)->
	indata = JSON.parse(fs.readFileSync(file))
	fix(indata)
	fs.writeFileSync file, JSON.stringify(indata, null, '\t')




