_Mozilla Hacks Post: https://hacks.mozilla.org_

# Songs of Diridum: Pushing the Web Audio API to Its Limits

> on October XX, 2013 by Oskar Åsbrink, Marcus Krüger and Tom Söderlund

## Introduction to the Web Audio API

##What the webaudio api can do for you
I view the audio systems from four perspectives. These are game design, audio engineering,  programming and music production. For this article we will focus on the first three as these have been involved with our project. The topic of music production is also interesting but not very relevant here.

As a game designer I can use the functionality of the webaudio api to tune the soundscape of my game. I can run a whole lot of separate sounds simultaneously while also adjusting their character to fit an environment or a game mechanic. You can have muffled sounds coming through a closed door and open the filters for these sounds to unmuffle them gradually as the door open. In real time. I can add reflecting sounds of the environment to the footsteps of my character as I walk from a sidewalk into a church. The ambient sounds of the street will be echoing around in the church together with my footsteps. I can attach the sounds of a roaring fire to my magicians fireball, hurl it away and hear the fireball moving towards its target. I can hear the siren of a police car approaching and hear how it passes by from the pitch shift known as doppler effect. And I know I can use these features without needing to manage the production of an audio engine. Its already there and it works.

As an audio engineer I view the webaudio api as a big patch bay with a slew of outboard gear tape stations and mixers. On a low level I feel reasonably comfortable with the fundamental aspects of the system. I can work comfortably with changing the volume of a sound while it is playing without running the risk of inducing digital distortion from the volume level changing from one sample to another. The system will make the interpolation needed for this type of adjustment. I can also build the type of effects I want and hook them up however I want. As long as I keep my system reasonably small I can make a nice studio with the webaudio api.

As a programmer I can write the code needed for my project with ease. If I run into a problem I will usually find a good solution to it on the web. I don’t have to spend my time learning how to work with some proprietary audio engine without documentation. The type of problem I will spend most of my time with is how to structure my code. I will be figuring out how to handle the loading of the sounds and which sounds to load when. How to provide these sounds to the game designer through some suitable data structure or other design pipeline. I will also work with my team to figure out how to handle the budgeting of the sound system. How much data can we use? How many sounds can we play at the same time? How many effects can we use on the target platform. It is likely that my hardest problem will become related to handling the diversity of hardware and browsers running on the web.


## Introduction to the Goo Engine

* We should mention Goo Engine & Goo Create and how that helps developers create similar experiences as this demo. We should hint on the forthcoming release announcement in November.
* As per request from Mozilla we should also mention that the open code infrastructure of Goo Create drives development of the open web and great HTML5 coding forward. (This help a lot if Mozilla is going to promote the post in their channels)

## Building Songs of Diridum

### Using Web Audio in a 3D world

To build the soundscape of a 3D world we have access to spatialization of sound sources and the ears of the player. The spatial aspects of sounds and listeners are boiled down to position, direction and velocity. Each sound can also be made to emit its sound in a directional cone, in short this emulates the difference between the front and back of loudspeaker. A piano would not really need any directionality as it sounds quite similar in all directions. A megaphone on the other hand is comparably directional and should be louder at the front than at the back. 

The position of the sound is used to determine panning or which speaker the sound is louder in and how loud the sound should be.

The velocity of the sound and the listener together with their positions provide the information needed to doppler shift all sound sources in the world accordingly. For other worldly effects such as muffling sounds behind a door or changing the sound of the room with reverbs we’ll have to write some code and configure processing nodes to meet with our desired results.

For adding sounds to the ui and such direct effects we can hook the sounds up without going through the spatialization. Which makes them a bit simpler. You can still process the effects and be creative if you like. Perhaps pan UI the sound source based on where the mouse pointer clicked it.

### Initializing Web Audio

Setting up for using webaudio is quite simple. The tricky parts are related to figuring out how much sound you need to preload and how much you feel comfortable with loading later. You also need to take into account that loading a sound contains two asynchronous and potentially slow operations. The first is the download and the second is the decompression from some small format such as ogg or mp3 to arraybuffer. When developing against a local machine you’ll find that the decompression is a lot slower than the download and as with download speeds in general we can expect to not know how much time this will require for any given user.

### Playing sound streams


Once you have a sound decompressed and ready it can be used to create a sound source. A sound source is a relatively low level object which streams its sound data at some speed to its selected target node. For the simplest system this target node is the speaker output. Even with this primitive system you can already manipulate the playback rate of the sound, this changes its pitch and duration. There is a nice feature in the webaudio api which allows you to adjust the behaviour of interpolating a change like this to fit your desires.


### Adding sound effects: reverb, delay, and distortion

To add an effect to our simple system you put a processor node between the source and the speakers. us audio engineers wants to split the source to have a “dry” and a “wet” component at this point. The dry component is the non-processed sound and the wet is processed. Then you’ll send both the wet and the dry to the speakers and adjust the mix between them by adding a gain node on each of these tracks. Gain is an engineery way of saying volume. You can keep on like this and add nodes between the source and the speakers as you please. Sometimes you’ll want effects in parallel and sometimes you want them serially. When coding your system its probably a good idea to make it easy to change how this wiring is hooked up for any given node.

### Regarding mobile devices

When it comes to mobile devices I find that they cause complications on a general level for all kinds of webapps. The webaudio api is not different in this regard and you should expect that most mobile devices will present their own unique challenges to your project. The more moden Android and IOS devices seem to support webaudio rather well, altho they will have quite a lot capacity for heavy lifting when it comes to running effects or simultaneous tracks. Our brief testing shows that a decent desktop can easily handle more than 10 times as much audio processing as the best mobile device we had available, in our case a Nexus 4 had the most to give.