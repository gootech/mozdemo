var requirejs = require('requirejs');

requirejs.config({
	nodeRequire : require,
});

requirejs([], function() {
	var express = require('express');
	var app = express();
	var server = require('http').createServer(app);

	app.use(express.compress());

	var path = "C:\\code\\GooCode\\GooJS"; // put your own path here

	app.use('/js/goo/lib', express.static(path + "\\lib"));
	app.use('/js/goo', express.static(path + "\\src\\goo"));

	app.use('/mozdemo', express.static('C:\\code\\GooCode\\mozdemo')); // put your own path here

	app.listen(8081); // whatever port you prefer
});