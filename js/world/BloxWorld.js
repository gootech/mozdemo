define([
	'goo/renderer/Material',
	'goo/renderer/Shader',
	'goo/renderer/TextureCreator',
	'goo/renderer/MeshData',
	'goo/entities/components/MeshDataComponent',
	'goo/entities/components/MeshRendererComponent',
	'goo/renderer/Texture',
	'goo/renderer/bounds/BoundingBox',
	'goo/renderer/shaders/ShaderLib',
	'goo/renderer/shaders/ShaderBuilder',
	'goo/math/Vector3',
	'js/world/AtlasNode',
	'js/world/Tracer',

	'goo/shapes/ShapeCreator'
], function(
	Material,
	Shader,
	TextureCreator,
	MeshData,
	MeshDataComponent,
	MeshRendererComponent,
	Texture,
	BoundingBox,
	ShaderLib,
	ShaderBuilder,
	Vector3,
	AtlasNode,
	Tracer,

	ShapeCreator
) {
	"use strict";

	/**
	 * @name BloxWorld
	 * @class The purpose of this class is to hold and manage all block chunks
	 * @param {GooRunner} goo
	 * @param {int} worldWidth
	 * @param {int} worldHeight
	 * @param {int} chunkWidth
	 * @param {int} chunkHeight
	 * @param {Array} types
	 */
	var lightmapDensity = 1;
	// var lightmapDensity = 4;
	var lightmapSize = 1024;
	var shadowStrength = 12.0;
	var suntraceLength = 40.0;

	function BloxWorld(goo, worldWidth, worldHeight, dims, types) {
		this.goo = goo;

		var material = this.material = Material.createMaterial(createShader());
		var texture = this.generateTexture(types);
		material.setTexture(Shader.DIFFUSE_MAP, texture);
		material.uniforms.materialAmbient = [0.2, 0.2, 0.3, 1.0];
		material.uniforms.materialSpecular = [0.4, 0.4, 0.4, 1.0];
		material.uniforms.materialSpecularPower = 32;

		var material2 = this.material2 = Material.createMaterial(ShaderLib.simple);
		material2.wireframe = true;
		material2.wireframeColor = [0, 0, 0];

		this.loaded = false;

		this.entity = goo.world.createEntity();
		this.entity.addToWorld();

		this.dims = dims;
		var size = dims[0] * dims[1] * dims[2];

		this.worldWidth = worldWidth;
		this.worldHeight = worldHeight;
	}

	BloxWorld.prototype.generateTexture = function(types) {
		var colorInfo = new Uint8Array(4 * 256 /*types.length*/ );

		for (var i = 0; i < types.length; i++) {
			var type = types[i];
			colorInfo[i * 4 + 0] = Math.floor(type.r * 255);
			colorInfo[i * 4 + 1] = Math.floor(type.g * 255);
			colorInfo[i * 4 + 2] = Math.floor(type.b * 255);
			colorInfo[i * 4 + 3] = 255;

			colorInfo[i * 4 + 0] = Math.floor(type.r * 255);
			colorInfo[i * 4 + 1] = Math.floor(type.g * 255);
			colorInfo[i * 4 + 2] = Math.floor(type.b * 255);
		}
		for (var i = types.length; i < 256; i++) {
			colorInfo[i * 4 + 0] = 155;
			colorInfo[i * 4 + 1] = 0;
			colorInfo[i * 4 + 2] = 155;
			colorInfo[i * 4 + 3] = 255;
		}

		var texture = new Texture(colorInfo, null, 256, 1);
		texture.minFilter = 'NearestNeighborNoMipMaps';
		texture.magFilter = 'NearestNeighbor';
		texture.generateMipmaps = false;
		return texture;
	};

	BloxWorld.prototype.buildEntities = function(data) {
		// console.profile();
		var rootEntity = this.goo.world.createEntity('root');
		rootEntity.addToWorld();
		var meshes = this.buildQuads(data, this.dims);
		for (var meshIndex = 0; meshIndex < meshes.length; meshIndex++) {
			var quadData = meshes[meshIndex];

			var faceCount = quadData.faceCount;

			// var tracer = new Tracer({
			// 	getBlock: function(x, y, z) {
			// 		if (x < 0 || x >= this.dims[0] || y < 0 || y >= this.dims[1] || z < 0 || z >= this.dims[2]) {
			// 			return 0;
			// 		}
			// 		var index = x + (y + z * this.dims[1]) * this.dims[0];
			// 		return data[index];
			// 	}.bind(this)
			// });
			// //TODO: step through all pixels
			// var colorInfo = new Uint8Array(lightmapSize * lightmapSize * 4);

			// for (var i = 0; i < faceCount; i++) {
			// 	var xn = quadData.normals[i * 16 + 0] - 1;
			// 	var yn = quadData.normals[i * 16 + 1] - 1;
			// 	var zn = quadData.normals[i * 16 + 2] - 1;


			// 	u1 = quadData.uv0[i * 8 + 0];
			// 	u2 = quadData.uv0[i * 8 + 2];
			// 	if (u2 <= u1) {
			// 		u2 = quadData.uv0[i * 8 + 6];
			// 	}

			// 	v1 = quadData.uv0[i * 8 + 1];
			// 	v2 = quadData.uv0[i * 8 + 3];
			// 	if (v2 <= v1) {
			// 		v2 = quadData.uv0[i * 8 + 7];
			// 	}

			// 	var stepLength = 1 / lightmapDensity;
			// 	var stepLengthUV = 1 / lightmapSize;
			// 	var u = u1;
			// 	// console.log('face', i);
			// 	// console.log('\tcoords', x1, y1, x2, y2);
			// 	// console.log('\tuv', u1, v1, u2, v2);
			// 	var curpos = new Vector3();
			// 	var raydir = new Vector3();
			// 	var iterations = 8;
			// 	var result = {};
			// 	console.log('face', i);

			// 	var x1, x2, y1, y2;
			// 	var u1, u2, v1, v2;
			// 	if (xn !== 0) {
			// 		var x0 = quadData.vertices[i * 12 + 0];
			// 		// console.log('xn');
			// 		x1 = quadData.vertices[i * 12 + 1]; // y
			// 		x2 = quadData.vertices[i * 12 + 4];
			// 		if (x2 <= x1) {
			// 			x2 = quadData.vertices[i * 12 + 10];
			// 		}
			// 		y1 = quadData.vertices[i * 12 + 2]; // z
			// 		y2 = quadData.vertices[i * 12 + 5];
			// 		if (y2 <= y1) {
			// 			y2 = quadData.vertices[i * 12 + 11];
			// 		}

			// 		var offset = xn * 0.5;
			// 		for (var x = x1; x < x2; x += stepLength) {
			// 			var v = v1;
			// 			for (var y = y1; y < y2; y += stepLength) {
			// 				var light = 0.0;

			// 				curpos.setd(x0 + offset, x + stepLength / 2, y + stepLength / 2);

			// 				var directLight = false;
			// 				if (xn > 0) {
			// 					raydir.setd(1, 1, 0.5);
			// 					raydir.normalize();
			// 					tracer.traceCollision(curpos, raydir, suntraceLength, result);
			// 					if (!result.hit) {
			// 						directLight = true;
			// 						light = shadowStrength;
			// 					}
			// 				}

			// 				if (!directLight) {
			// 					raydir.setd(xn, 0, 0);
			// 					light += trace(tracer, curpos, raydir, iterations, result);

			// 					raydir.setd(xn, 1, 1);
			// 					raydir.normalize();
			// 					light += trace(tracer, curpos, raydir, iterations, result);
			// 					raydir.setd(xn, 1, -1);
			// 					raydir.normalize();
			// 					light += trace(tracer, curpos, raydir, iterations, result);
			// 					raydir.setd(xn, -1, 1);
			// 					raydir.normalize();
			// 					light += trace(tracer, curpos, raydir, iterations, result);
			// 					raydir.setd(xn, -1, -1);
			// 					raydir.normalize();
			// 					light += trace(tracer, curpos, raydir, iterations, result);

			// 					raydir.setd(xn, 10, 0);
			// 					raydir.normalize();
			// 					light += trace(tracer, curpos, raydir, iterations, result);
			// 					raydir.setd(xn, -10, 0);
			// 					raydir.normalize();
			// 					light += trace(tracer, curpos, raydir, iterations, result);
			// 					raydir.setd(xn, 0, 10);
			// 					raydir.normalize();
			// 					light += trace(tracer, curpos, raydir, iterations, result);
			// 					raydir.setd(xn, 0, -10);
			// 					raydir.normalize();
			// 					light += trace(tracer, curpos, raydir, iterations, result);
			// 				}

			// 				light *= 255.0 / shadowStrength;

			// 				var index = (v * lightmapSize * lightmapSize + u * lightmapSize) * 4;
			// 				colorInfo[index + 0] = light;
			// 				colorInfo[index + 1] = light;
			// 				colorInfo[index + 2] = light;
			// 				colorInfo[index + 3] = 255;

			// 				v += stepLengthUV;
			// 			}
			// 			u += stepLengthUV;
			// 		}
			// 	} else if (yn !== 0) {
			// 		var x0 = quadData.vertices[i * 12 + 1];

			// 		// console.log('yn');
			// 		x1 = quadData.vertices[i * 12 + 0]; // x
			// 		x2 = quadData.vertices[i * 12 + 3];
			// 		if (x2 <= x1) {
			// 			x2 = quadData.vertices[i * 12 + 9];
			// 		}
			// 		y1 = quadData.vertices[i * 12 + 2]; // z
			// 		y2 = quadData.vertices[i * 12 + 5];
			// 		if (y2 <= y1) {
			// 			y2 = quadData.vertices[i * 12 + 11];
			// 		}

			// 		var offset = yn * 0.5;
			// 		for (var x = x1; x < x2; x += stepLength) {
			// 			var v = v1;
			// 			for (var y = y1; y < y2; y += stepLength) {
			// 				var light = 0.0;

			// 				curpos.setd(x + stepLength / 2, x0 + offset, y + stepLength / 2);

			// 				var directLight = false;
			// 				if (yn > 0) {
			// 					raydir.setd(1, 1, 0.5);
			// 					raydir.normalize();
			// 					tracer.traceCollision(curpos, raydir, suntraceLength, result);
			// 					if (!result.hit) {
			// 						directLight = true;
			// 						light = shadowStrength * 0.95;
			// 					}
			// 				}

			// 				if (!directLight) {
			// 					raydir.setd(0, yn, 0);
			// 					light += trace(tracer, curpos, raydir, iterations, result);

			// 					raydir.setd(-1, yn, -1);
			// 					raydir.normalize();
			// 					light += trace(tracer, curpos, raydir, iterations, result);
			// 					raydir.setd(-1, yn, 1);
			// 					raydir.normalize();
			// 					light += trace(tracer, curpos, raydir, iterations, result);
			// 					raydir.setd(1, yn, -1);
			// 					raydir.normalize();
			// 					light += trace(tracer, curpos, raydir, iterations, result);
			// 					raydir.setd(1, yn, 1);
			// 					raydir.normalize();
			// 					light += trace(tracer, curpos, raydir, iterations, result);

			// 					raydir.setd(10, yn, 0);
			// 					raydir.normalize();
			// 					light += trace(tracer, curpos, raydir, iterations, result);
			// 					raydir.setd(-10, yn, 0);
			// 					raydir.normalize();
			// 					light += trace(tracer, curpos, raydir, iterations, result);
			// 					raydir.setd(0, yn, 10);
			// 					raydir.normalize();
			// 					light += trace(tracer, curpos, raydir, iterations, result);
			// 					raydir.setd(0, yn, -10);
			// 					raydir.normalize();
			// 					light += trace(tracer, curpos, raydir, iterations, result);
			// 				}

			// 				light *= 255.0 / shadowStrength;

			// 				var index = (v * lightmapSize * lightmapSize + u * lightmapSize) * 4;
			// 				colorInfo[index + 0] = light;
			// 				colorInfo[index + 1] = light;
			// 				colorInfo[index + 2] = light;
			// 				colorInfo[index + 3] = 255;

			// 				v += stepLengthUV;
			// 			}
			// 			u += stepLengthUV;
			// 		}
			// 	} else if (zn !== 0) {
			// 		var x0 = quadData.vertices[i * 12 + 2];

			// 		// console.log('zn');
			// 		x1 = quadData.vertices[i * 12 + 0]; // x
			// 		x2 = quadData.vertices[i * 12 + 3];
			// 		if (x2 <= x1) {
			// 			x2 = quadData.vertices[i * 12 + 9];
			// 		}
			// 		y1 = quadData.vertices[i * 12 + 1]; // y
			// 		y2 = quadData.vertices[i * 12 + 4];
			// 		if (y2 <= y1) {
			// 			y2 = quadData.vertices[i * 12 + 10];
			// 		}

			// 		var offset = zn * 0.5;
			// 		for (var x = x1; x < x2; x += stepLength) {
			// 			var v = v1;
			// 			for (var y = y1; y < y2; y += stepLength) {
			// 				var light = 0.0;

			// 				curpos.setd(x + stepLength / 2, y + stepLength / 2, x0 + offset);

			// 				var directLight = false;
			// 				if (zn > 0) {
			// 					raydir.setd(1, 1, 0.5);
			// 					raydir.normalize();
			// 					tracer.traceCollision(curpos, raydir, suntraceLength, result);
			// 					if (!result.hit) {
			// 						directLight = true;
			// 						light = shadowStrength * 0.9;
			// 					}
			// 				}

			// 				if (!directLight) {
			// 					raydir.setd(0, 0, zn);
			// 					light += trace(tracer, curpos, raydir, iterations, result);

			// 					raydir.setd(-1, -1, zn);
			// 					raydir.normalize();
			// 					light += trace(tracer, curpos, raydir, iterations, result);
			// 					raydir.setd(1, -1, zn);
			// 					raydir.normalize();
			// 					light += trace(tracer, curpos, raydir, iterations, result);
			// 					raydir.setd(-1, 1, zn);
			// 					raydir.normalize();
			// 					light += trace(tracer, curpos, raydir, iterations, result);
			// 					raydir.setd(1, 1, zn);
			// 					raydir.normalize();
			// 					light += trace(tracer, curpos, raydir, iterations, result);

			// 					raydir.setd(10, 0, zn);
			// 					raydir.normalize();
			// 					light += trace(tracer, curpos, raydir, iterations, result);
			// 					raydir.setd(-10, 0, zn);
			// 					raydir.normalize();
			// 					light += trace(tracer, curpos, raydir, iterations, result);
			// 					raydir.setd(0, 10, zn);
			// 					raydir.normalize();
			// 					light += trace(tracer, curpos, raydir, iterations, result);
			// 					raydir.setd(0, -10, zn);
			// 					raydir.normalize();
			// 					light += trace(tracer, curpos, raydir, iterations, result);
			// 				}

			// 				light *= 255.0 / shadowStrength;

			// 				var index = (v * lightmapSize * lightmapSize + u * lightmapSize) * 4;
			// 				colorInfo[index + 0] = light;
			// 				colorInfo[index + 1] = light;
			// 				colorInfo[index + 2] = light;
			// 				colorInfo[index + 3] = 255;

			// 				v += stepLengthUV;
			// 			}
			// 			u += stepLengthUV;
			// 		}
			// 	}
			// }

			// var lightmapSizeHalf = lightmapSize / 2;
			// var colorInfoDown = new Uint8Array(lightmapSizeHalf * lightmapSizeHalf * 4);
			// for (var u = 0; u < lightmapSizeHalf; u++) {
			// 	for (var v = 0; v < lightmapSizeHalf; v++) {
			// 		var index1 = (v * lightmapSize * 2 + u * 2) * 4;
			// 		var index2 = (v * lightmapSizeHalf + u) * 4;
					
			// 		var divider = 0;
			// 		for (var x=0;x<2;x++) {
			// 			for (var y=0;y<2;y++) {
			// 				var adder = index1 + (y * lightmapSize + x) * 4;
			// 				var alpha = colorInfo[adder + 3];
			// 				if (alpha > 128) {
			// 					colorInfoDown[index2 + 0] += colorInfo[adder + 0];
			// 					colorInfoDown[index2 + 1] += colorInfo[adder + 1];
			// 					colorInfoDown[index2 + 2] += colorInfo[adder + 2];
			// 					divider++;
			// 				}
			// 			}
			// 		}
			// 		if (divider > 0) {
			// 			colorInfoDown[index2 + 0] /= divider;
			// 			colorInfoDown[index2 + 1] /= divider;
			// 			colorInfoDown[index2 + 2] /= divider;
			// 		}
			// 	}
			// }

			// for (var u = 0; u < lightmapSize; u++) {
			// 	for (var v = 0; v < lightmapSize; v++) {
			// 		var index1 = (v * lightmapSize + u) * 4;
			// 		var index2 = (Math.floor(v / 2) * lightmapSizeHalf + Math.floor(u / 2)) * 4;

			// 		var alpha = colorInfo[index1 + 3];
			// 		if (alpha < 128) {
			// 			colorInfo[index1 + 0] = colorInfoDown[index2 + 0];
			// 			colorInfo[index1 + 1] = colorInfoDown[index2 + 1];
			// 			colorInfo[index1 + 2] = colorInfoDown[index2 + 2];
			// 		}
			// 	}
			// }

			// var texture2 = new Texture(colorInfo, {
				// flipY: false
			// }, lightmapSize, lightmapSize);
			// var texture2 = new Texture(colorInfoDown, {
			// 	flipY: false
			// }, lightmapSizeHalf, lightmapSizeHalf);
			// texture2.minFilter = 'NearestNeighborNoMipMaps';
			// texture2.magFilter = 'NearestNeighbor';
			// texture2.generateMipmaps = false;
			// this.material.setTexture(Shader.LIGHT_MAP, texture2);
			// createDebugQuad(this.goo, texture2);

			var attributeMap = {
				POSITION: MeshData.createAttribute(3, 'Float'),
				// TEXCOORD0: MeshData.createAttribute(2, 'Float'),
				NORMAL_COL: MeshData.createAttribute(4, 'UnsignedByte')
			};
			var meshData = new MeshData(attributeMap, faceCount * 4, faceCount * 6);
			meshData.wireframeData = null;

			if (faceCount > 0) {
				meshData.getAttributeBuffer('POSITION').set(quadData.vertices);
				// meshData.getAttributeBuffer('TEXCOORD0').set(quadData.uv0);
				meshData.getAttributeBuffer('NORMAL_COL').set(quadData.normals);
				meshData.getIndexBuffer().set(quadData.faces);
			}

			var entity = this.createChunkEntity(meshData);
			rootEntity.transformComponent.attachChild(entity.transformComponent);
		}

		this.building = false;

		return rootEntity;
	};

	function trace(tracer, curpos, raydir, iterations, result) {
		tracer.traceCollision(curpos, raydir, iterations, result);
		if (result.hit) {
			return result.length / iterations;
		}

		return 1.0;
	}

	function createDebugQuad(goo, texture) {
		var world = goo.world;
		var entity = world.createEntity('Quad');
		entity.transformComponent.transform.translation.set(0, 0, 0);

		var quad = ShapeCreator.createQuad(2, 2);
		var meshDataComponent = new MeshDataComponent(quad);
		entity.setComponent(meshDataComponent);

		var fsShader = {
			attributes: {
				vertexPosition: MeshData.POSITION
			},
			uniforms: {
				diffuseMap: Shader.DIFFUSE_MAP,
				resolution: Shader.RESOLUTION
			},
			vshader: [ //
				'attribute vec3 vertexPosition;', //
				'uniform vec2 resolution;',

				'const vec2 madd = vec2(0.5,0.5);',
				'varying vec2 textureCoord;',

				'void main(void) {', //
				'	textureCoord = vertexPosition.xy * madd + madd;', // scale vertex attribute to [0-1] range
				'	gl_Position = vec4(step(vec2(0.0), vertexPosition.xy)/resolution * vec2(256.0) - vec2(0.75), 0.0, 1.0);',
				'}' //
			].join('\n'),
			fshader: [ //
				'precision mediump float;', //

				'uniform sampler2D diffuseMap;', //

				'varying vec2 textureCoord;',

				'void main(void)', //
				'{', //
				'	gl_FragColor = texture2D(diffuseMap,textureCoord);', //
				'}' //
			].join('\n')
		};

		var meshRendererComponent = new MeshRendererComponent();
		meshRendererComponent.cullMode = 'Never';
		var material = Material.createMaterial(fsShader, 'fsshader');
		meshRendererComponent.materials.push(material);
		entity.setComponent(meshRendererComponent);

		material.setTexture(Shader.DIFFUSE_MAP, texture);

		entity.addToWorld();
	}

	BloxWorld.prototype.createChunkEntity = function(meshData) {
		var world = this.goo.world;

		var entity = world.createEntity();
		var meshDataComponent = new MeshDataComponent(meshData);

		var boundingCenter = new Vector3(this.dims[0] * 0.5, this.dims[1] * 0.5, this.dims[2] * 0.5);
		meshDataComponent.setModelBound(new BoundingBox(boundingCenter, boundingCenter.x, boundingCenter.y, boundingCenter.z), false);
		entity.setComponent(meshDataComponent);

		var meshRendererComponent = new MeshRendererComponent();
		meshRendererComponent.castShadows = true;
		meshRendererComponent.materials.push(this.material);
		// meshRendererComponent.materials.push(this.material2); // wireframe
		entity.setComponent(meshRendererComponent);

		entity.addToWorld();

		return entity;
	};

	function createShader() {
		return {
			processors: [
				ShaderBuilder.light.processor
			],
			attributes: {
				vertexPosition: 'POSITION',
				// vertexUV0: 'TEXCOORD0',
				vertexNormal: 'NORMAL_COL'
			},
			uniforms: {
				viewMatrix: Shader.VIEW_MATRIX,
				projectionMatrix: Shader.PROJECTION_MATRIX,
				worldMatrix: Shader.WORLD_MATRIX,
				cameraPosition: Shader.CAMERA,
				diffuseMap: Shader.DIFFUSE_MAP,
				lightMap: Shader.LIGHT_MAP,
				fogColor: [1, 1, 1, 1]
			},
			vshader: [ //
				'attribute vec3 vertexPosition;', //
				// 'attribute vec2 vertexUV0;', //
				'attribute vec4 vertexNormal;', //

				'uniform mat4 viewMatrix;', //
				'uniform mat4 projectionMatrix;', //
				'uniform mat4 worldMatrix;', //

				'uniform vec3 cameraPosition;',

				'varying float col;', //
				// 'varying vec2 uv0;', //
				'varying vec3 norm;', //
				'varying vec4 vWorldPos;',
				'varying vec3 viewPosition;',

				ShaderBuilder.light.prevertex,

				'void main(void) {', //
					'norm = vertexNormal.xyz - vec3(1.0);', //
					'col = vertexNormal.w;', //

					// 'uv0 = vertexUV0;',

					'vec4 worldPos = worldMatrix * vec4(vertexPosition, 1.0);',

					'viewPosition = cameraPosition - worldPos.xyz;',

					'vWorldPos = worldPos;',
					'gl_Position = projectionMatrix * viewMatrix * vWorldPos;', //

					ShaderBuilder.light.vertex,
				'}' //
			].join('\n'),
			fshader: [ //
				'uniform mat4 viewMatrix;', //
				'uniform mat4 projectionMatrix;', //

				'uniform sampler2D diffuseMap;', //
				// 'uniform sampler2D lightMap;', //

				'uniform vec4 fogColor;', //

				'varying float col;', //
				// 'varying vec2 uv0;', //
				'varying vec3 norm;', //
				'varying vec4 vWorldPos;',
				'varying vec3 viewPosition;',

				'const float density = 0.005;', //
				'const float LOG2 = 1.442695;', //
				'const float PI = 3.1415926535897932384626;',

				ShaderBuilder.light.prefragment,

				'void main(void)', //
				'{', //
					// 'vec3 N = normalize(norm);',
					'vec3 N = norm;',
					'vec4 final_color = vec4(1.0);',

					'final_color.rgb *= texture2D(diffuseMap, vec2(col/255.0)).rgb * vec3(0.8) + vec3(0.2);', //
					// 'final_color.rgb = texture2D(lightMap, uv0).rgb;', //


					// ShaderBuilder.light.fragment,
					'#ifdef SPECULAR_MAP',
						'float specularStrength = texture2D(specularMap, texCoord0).x;',
					'#else',
						'float specularStrength = 1.0;',
					'#endif',

					"#if MAX_POINT_LIGHTS > 0",
						"vec3 pointDiffuse  = vec3(0.0);",
						"vec3 pointSpecular = vec3(0.0);",

						"for (int i = 0; i < MAX_POINT_LIGHTS; i++) {",
							'vec3 lVector = normalize(pointLight[i].xyz - vWorldPos.xyz);',
							"float lDistance = 1.0 - min((length(pointLight[i].xyz - vWorldPos.xyz) / pointLight[i].w), 1.0);",
							// "vec3 lVector = normalize(vPointLight[i].xyz);",
							// "float lDistance = vPointLight[i].w;",

							// diffuse
							"float dotProduct = dot(N, lVector);",

							"#ifdef WRAP_AROUND",
								"float pointDiffuseWeightFull = max(dotProduct, 0.0);",
								"float pointDiffuseWeightHalf = max(0.5 * dotProduct + 0.5, 0.0);",

								"vec3 pointDiffuseWeight = mix(vec3(pointDiffuseWeightFull), vec3(pointDiffuseWeightHalf), wrapRGB);",
							"#else",
								"float pointDiffuseWeight = max(dotProduct, 0.0);",
							"#endif",

							"pointDiffuse += materialDiffuse.rgb * pointLightColor[i].rgb * pointDiffuseWeight * lDistance;",

							// specular
							"vec3 pointHalfVector = normalize(lVector + normalize(viewPosition));",
							"float pointDotNormalHalf = max(dot(N, pointHalfVector), 0.0);",
							"float pointSpecularWeight = pointLightColor[i].a * specularStrength * max(pow(pointDotNormalHalf, materialSpecularPower), 0.0);",

							"#ifdef PHYSICALLY_BASED_SHADING",
								"float specularNormalization = (materialSpecularPower + 2.0001 ) / 8.0;",
								"vec3 schlick = materialSpecular.rgb + vec3(1.0 - materialSpecular.rgb) * pow(1.0 - dot(lVector, pointHalfVector), 5.0);",
								"pointSpecular += schlick * pointLightColor[i].rgb * pointSpecularWeight * pointDiffuseWeight * lDistance * specularNormalization;",
							"#else",
								"pointSpecular += materialSpecular.rgb * pointLightColor[i].rgb * pointSpecularWeight * pointDiffuseWeight * lDistance;",
							"#endif",
						"}",
					"#endif",

					"#if MAX_SPOT_LIGHTS > 0",
						"vec3 spotDiffuse  = vec3(0.0);",
						"vec3 spotSpecular = vec3(0.0);",

						"for (int i = 0; i < MAX_SPOT_LIGHTS; i++) {",
							'vec3 lVector = normalize(spotLight[i].xyz - vWorldPos.xyz);',
							"float lDistance = 1.0 - min((length(spotLight[i].xyz - vWorldPos.xyz) / spotLight[i].w), 1.0);",
							// "vec3 lVector = normalize(vSpotLight[i].xyz);",
							// "float lDistance = vSpotLight[i].w;",

							// "float spotEffect = dot( spotLightDirection[ i ], normalize( spotLightPosition[ i ] - vWorldPosition ) );",
							"float spotEffect = dot(normalize(-spotLightDirection[i]), lVector);",

							// 'spotDiffuse = vec3(spotEffect);',

							"if (spotEffect > spotLightAngle[i]) {",
								//"spotEffect = clamp(spotEffect/1.0, 1.0, 0.0);",

								"if (spotLightAngle[i] < spotLightPenumbra[i]) {",
									"spotEffect = (spotEffect-spotLightAngle[i])/(spotLightPenumbra[i] - spotLightAngle[i]);",
									"spotEffect = clamp(spotEffect, 0.0, 1.0);",
								"} else {",
									"spotEffect = 1.0;",
								"}",

								//"spotEffect = max(pow(spotEffect, spotLightExponent[i]), 0.0);",

								// diffuse
								"float dotProduct = dot(N, lVector);",

								"#ifdef WRAP_AROUND",
									"float spotDiffuseWeightFull = max(dotProduct, 0.0);",
									"float spotDiffuseWeightHalf = max(0.5 * dotProduct + 0.5, 0.0);",

									"vec3 spotDiffuseWeight = mix(vec3(spotDiffuseWeightFull), vec3(spotDiffuseWeightHalf), wrapRGB);",
								"#else",
									"float spotDiffuseWeight = max(dotProduct, 0.0);",
								"#endif",

								"spotDiffuse += materialDiffuse.rgb * spotLightColor[i].rgb * spotDiffuseWeight * lDistance * spotEffect;",

								// specular
								"vec3 spotHalfVector = normalize(lVector + normalize(viewPosition));",
								"float spotDotNormalHalf = max(dot(N, spotHalfVector), 0.0);",
								"float spotSpecularWeight = spotLightColor[i].a * specularStrength * max(pow(spotDotNormalHalf, materialSpecularPower), 0.0);",

								"#ifdef PHYSICALLY_BASED_SHADING",
									"float specularNormalization = (materialSpecularPower + 2.0001) / 8.0;",
									"vec3 schlick = materialSpecular.rgb + vec3(1.0 - materialSpecular.rgb) * pow(1.0 - dot(lVector, spotHalfVector), 5.0);",
									"spotSpecular += schlick * spotLightColor[i].rgb * spotSpecularWeight * spotDiffuseWeight * lDistance * specularNormalization * spotEffect;",
								"#else",
									"spotSpecular += materialSpecular.rgb * spotLightColor[i].rgb * spotSpecularWeight * spotDiffuseWeight * lDistance * spotEffect;",
								"#endif",
							"}",
						"}",
					"#endif",

					"#if MAX_DIRECTIONAL_LIGHTS > 0",
						"vec3 dirDiffuse  = vec3(0.0);",
						"vec3 dirSpecular = vec3(0.0);" ,

						"for(int i = 0; i < MAX_DIRECTIONAL_LIGHTS; i++) {",
							"vec4 lDirection = vec4(-directionalLightDirection[i], 0.0);",
							// "vec4 lDirection = viewMatrix * vec4( directionalLightDirection[ i ], 0.0 );",
							"vec3 dirVector = normalize(lDirection.xyz);",

							// diffuse
							"float dotProduct = dot(N, dirVector);",

							"#ifdef WRAP_AROUND",
								"float dirDiffuseWeightFull = max(dotProduct, 0.0);",
								"float dirDiffuseWeightHalf = max(0.5 * dotProduct + 0.5, 0.0);",

								"vec3 dirDiffuseWeight = mix(vec3(dirDiffuseWeightFull), vec3(dirDiffuseWeightHalf), wrapRGB);",
							"#else",
								"float dirDiffuseWeight = max(dotProduct, 0.0);",
							"#endif",

							"dirDiffuse += materialDiffuse.rgb * directionalLightColor[i].rgb * dirDiffuseWeight;",

							// specular
							"vec3 dirHalfVector = normalize(dirVector + normalize(viewPosition));",
							"float dirDotNormalHalf = max(dot(N, dirHalfVector), 0.0);",
							"float dirSpecularWeight = directionalLightColor[i].a * specularStrength * max(pow(dirDotNormalHalf, materialSpecularPower), 0.0);",

							"#ifdef PHYSICALLY_BASED_SHADING",
								"float specularNormalization = (materialSpecularPower + 2.0001) / 8.0;",
								//"dirSpecular += materialSpecular.rgb * directionalLightColor[ i ] * dirSpecularWeight * dirDiffuseWeight * specularNormalization * fresnel;",
								"vec3 schlick = materialSpecular.rgb + vec3(1.0 - materialSpecular.rgb) * pow(1.0 - dot(dirVector, dirHalfVector), 5.0);",
								"dirSpecular += schlick * directionalLightColor[i].rgb * dirSpecularWeight * dirDiffuseWeight * specularNormalization;",
							"#else",
								"dirSpecular += materialSpecular.rgb * directionalLightColor[i].rgb * dirSpecularWeight * dirDiffuseWeight;",
							"#endif",
						"}",
					"#endif",

					"vec3 totalDiffuse = vec3(0.0);",
					"vec3 totalSpecular = vec3(0.0);",

					"#if MAX_DIRECTIONAL_LIGHTS > 0",
						"totalDiffuse += dirDiffuse;",
						"totalSpecular += dirSpecular;",
					"#endif",
					"#if MAX_POINT_LIGHTS > 0",
						"totalDiffuse += pointDiffuse;",
						"totalSpecular += pointSpecular;",
					"#endif",
					"#if MAX_SPOT_LIGHTS > 0",
						"totalDiffuse += spotDiffuse;",
						"totalSpecular += spotSpecular;",
					"#endif",

					'float shadow = 1.0;',
					'#ifdef SHADOW_MAP',
						'vec3 depth = lPosition.xyz / lPosition.w;',
						'depth.z = length(vWorldPos.xyz - lightPos) * cameraScale;',

						'if (depth.x >= 0.0 && depth.x <= 1.0 && depth.y >= 0.0 && depth.y <= 1.0 && depth.z >= 0.0 && depth.z <= 1.0) {',
							'#if SHADOW_TYPE == 0',
								'depth.z *= 0.99;',
								'float shadowDepth = texture2D(shadowMap, depth.xy).x;',
								'if ( depth.z > shadowDepth ) shadow = 0.5;',
							'#elif SHADOW_TYPE == 1',
								'vec4 texel = texture2D(shadowMap, depth.xy);',
								'vec2 moments = vec2(texel.x, texel.y);',
								'shadow = ChebychevInequality(moments, depth.z);',
								// 'shadow = VsmFixLightBleed(shadow, 0.1);',
								// 'shadow = pow(shadow, 8.0);',
							'#endif',
							'shadow = clamp(shadow, 0.0, 1.0);',
						'}',
					'#endif',

					"vec3 ambientLightColor = vec3(1.0, 1.0, 1.0);",
					"#ifdef METAL",
						"final_color.xyz = final_color.xyz * (materialEmissive.rgb + totalDiffuse * shadow + ambientLightColor * materialAmbient.rgb + totalSpecular * shadow);",
					"#else",
						"final_color.xyz = final_color.xyz * (materialEmissive.rgb + totalDiffuse * 1.2 * shadow + ambientLightColor * materialAmbient.rgb) + totalSpecular * shadow;",
					"#endif",

					'	float z = gl_FragCoord.z / gl_FragCoord.w;', //
					'	float fogFactor = exp2( -density * ', //
					'				   density * ', //
					'				   z * ', //
					'				   z * ', //
					'				   LOG2 );', //
					'	fogFactor = clamp(fogFactor, 0.0, 1.0);', //

					'	gl_FragColor = mix(fogColor, final_color, fogFactor );', //
					// 'gl_FragColor = final_color;', //
				'}'
			].join('\n')
		};
	}

	var mask = new Int32Array(20 * 20);
	BloxWorld.prototype.buildQuads = function(volume, dims) {
		// var atlasNode = new AtlasNode(lightmapSize, lightmapSize);

		// var offset = 0.0; //0.01 / lightmapSize;

		// Sweep over 3-axes
		var meshes = [];
		var currentMesh = {
			faceCount: 0,
			vertices: [],
			// uv0: [],
			faces: [],
			normals: []
		};
		meshes.push(currentMesh);
		for (var d = 0; d < 3; ++d) {
			var i, j, k, l, w, h;
			var u = (d + 1) % 3;
			var v = (d + 2) % 3;
			var x = [0, 0, 0];
			var q = [0, 0, 0];

			if (mask.length < dims[u] * dims[v]) {
				mask = new Int32Array(dims[u] * dims[v]);
			}
			q[d] = 1;

			for (x[d] = -1; x[d] < dims[d];) {
				// Compute mask
				var n = 0;
				for (x[v] = 0; x[v] < dims[v]; ++x[v]) {
					for (x[u] = 0; x[u] < dims[u]; ++x[u], ++n) {
						var a = 0 <= x[d] ?
							volume[x[0] + dims[0] * (x[1] + dims[1] * x[2])] :
							0,
							b = x[d] < dims[d] - 1 ?
								volume[x[0] + q[0] + dims[0] * (x[1] + q[1] + dims[1] * (x[2] + q[2]))] :
								0;
						if ( !! a === !! b) {
							mask[n] = 0;
						} else if ( !! a) {
							mask[n] = a;
						} else {
							mask[n] = -b;
						}
					}
				}
				// Increment x[d]
				++x[d];
				// Generate mesh for mask using lexicographic ordering
				n = 0;
				for (j = 0; j < dims[v]; ++j) {
					for (i = 0; i < dims[u];) {
						var c = mask[n];
						if ( !! c) {
							// Compute width
							for (w = 1; c === mask[n + w] && i + w < dims[u]; ++w) {}
							// Compute height (this is slightly awkward
							var done = false;
							for (h = 1; j + h < dims[v]; ++h) {
								for (k = 0; k < w; ++k) {
									if (c !== mask[n + k + h * dims[u]]) {
										done = true;
										break;
									}
								}
								if (done) {
									break;
								}
							}
							// Add quad
							x[u] = i;
							x[v] = j;
							var du = [0, 0, 0],
								dv = [0, 0, 0];
							if (c > 0) {
								dv[v] = h;
								du[u] = w;
							} else {
								c = -c;
								du[v] = h;
								dv[u] = w;
							}


							var normalX = du[0] || dv[0] ? 1 : du[1] === 0 ? 0 : 2;
							var normalY = du[1] || dv[1] ? 1 : du[2] === 0 ? 0 : 2;
							var normalZ = du[2] || dv[2] ? 1 : du[0] === 0 ? 0 : 2;

							// var nn;
							// var xx = du[0] || dv[0];
							// var yy = du[1] || dv[1];
							// var zz = du[2] || dv[2];
							// var xxx, yyy;
							// if (xx === 0) {
							// 	xxx = yy;
							// 	yyy = zz;
							// 	nn = normalX;
							// } else if (yy === 0) {
							// 	xxx = xx;
							// 	yyy = zz;
							// 	nn = normalY;
							// } else if (zz === 0) {
							// 	xxx = xx;
							// 	yyy = yy;
							// 	nn = normalZ;
							// }

							var vertices = currentMesh.vertices;
							var normals = currentMesh.normals;
							var faces = currentMesh.faces;

							x[0] = Math.round(x[0]);
							x[1] = Math.round(x[1]);
							x[2] = Math.round(x[2]);
							du[0] = Math.round(du[0]);
							du[1] = Math.round(du[1]);
							du[2] = Math.round(du[2]);

							vertices.push(x[0]);
							vertices.push(x[1]);
							vertices.push(x[2]);
							vertices.push(x[0] + du[0]);
							vertices.push(x[1] + du[1]);
							vertices.push(x[2] + du[2]);
							vertices.push(x[0] + du[0] + dv[0]);
							vertices.push(x[1] + du[1] + dv[1]);
							vertices.push(x[2] + du[2] + dv[2]);
							vertices.push(x[0] + dv[0]);
							vertices.push(x[1] + dv[1]);
							vertices.push(x[2] + dv[2]);

							// var rect = atlasNode.insert(xxx * lightmapDensity, yyy * lightmapDensity);
							// if (rect) {
							// 	rect = rect.localRectangle;
							// } else {
							// 	console.warn('Did not fit: ', du, dv);
							// 	rect = {
							// 		x: 0,
							// 		y: 0,
							// 		w: 2,
							// 		h: 2
							// 	};
							// }

							// if ((xx === 0 && nn === 2) || (yy === 0 && nn === 0) || (zz === 0 && nn === 2)) {
							// 	uv0.push((rect.x / lightmapSize) + offset);
							// 	uv0.push((rect.y / lightmapSize) + offset);
							// 	uv0.push(((rect.x + rect.w) / lightmapSize) - offset);
							// 	uv0.push((rect.y / lightmapSize) + offset);
							// 	uv0.push(((rect.x + rect.w) / lightmapSize) - offset);
							// 	uv0.push(((rect.y + rect.h) / lightmapSize) - offset);
							// 	uv0.push((rect.x / lightmapSize) + offset);
							// 	uv0.push(((rect.y + rect.h) / lightmapSize) - offset);
							// } else {
							// 	uv0.push((rect.x / lightmapSize) + offset);
							// 	uv0.push((rect.y / lightmapSize) + offset);
							// 	uv0.push((rect.x / lightmapSize) + offset);
							// 	uv0.push(((rect.y + rect.h) / lightmapSize) - offset);
							// 	uv0.push(((rect.x + rect.w) / lightmapSize) - offset);
							// 	uv0.push(((rect.y + rect.h) / lightmapSize) - offset);
							// 	uv0.push(((rect.x + rect.w) / lightmapSize) - offset);
							// 	uv0.push((rect.y / lightmapSize) + offset);
							// }

							for (var normalIndex = 0; normalIndex < 4; normalIndex++) {
								normals.push(normalX);
								normals.push(normalY);
								normals.push(normalZ);
								normals.push(c-1);
							}

							var vertex_count = currentMesh.faceCount * 4;
							faces.push(vertex_count);
							faces.push(vertex_count + 1);
							faces.push(vertex_count + 2);
							faces.push(vertex_count + 2);
							faces.push(vertex_count + 3);
							faces.push(vertex_count + 0);

							currentMesh.faceCount++;
							if (currentMesh.faceCount * 4 > 65000) {
								currentMesh = {
									faceCount: 0,
									vertices: [],
									// uv0: [],
									faces: [],
									normals: []
								};
								meshes.push(currentMesh);
							}

							// Zero-out mask
							for (l = 0; l < h; ++l) {
								for (k = 0; k < w; ++k) {
									mask[n + k + l * dims[u]] = 0;
								}
							}
							// Increment counters and continue
							i += w;
							n += w;
						} else {
							++i;
							++n;
						}
					}
				}
			}
		}

		// var rectangles = atlasNode.getRectangles();
		// var canvasSize = 128;
		// var canvas = document.createElement('canvas');
		// canvas.style.position = 'absolute';
		// canvas.style.zIndex = 100;
		// canvas.style.left = '200px';
		// canvas.style.width = canvasSize + 'px';
		// canvas.style.height = canvasSize + 'px';
		// canvas.width = canvasSize;
		// canvas.height = canvasSize;
		// var ctx = canvas.getContext('2d');
		// ctx.fillStyle = "#cccccc";
		// ctx.fillRect(0, 0, canvasSize, canvasSize);
		// ctx.fillStyle = "#000000";
		// ctx.lineWidth = 1;
		// for (var i = 0; i < rectangles.length; i++) {
		// 	var rectangle = rectangles[i];
		// 	// ctx.fillRect(rectangle.x, rectangle.y, rectangle.w, rectangle.h);
		// 	ctx.strokeRect(
		// 		Math.floor(rectangle.x * canvasSize / lightmapSize) + 1.5,
		// 		Math.floor(rectangle.y * canvasSize / lightmapSize) + 1.5,
		// 		Math.floor(rectangle.w * canvasSize / lightmapSize) - 2,
		// 		Math.floor(rectangle.h * canvasSize / lightmapSize) - 2);
		// }
		// document.body.appendChild(canvas);

		console.log(meshes);

		return meshes;
	};

	return BloxWorld;
});